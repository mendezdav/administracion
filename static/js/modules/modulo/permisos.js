var permisosApp = angular.module('permisosApp',[]);	

permisosApp.controller('permisosController', function($scope, $http){
	$scope.permisosDisponibles = [];
	$scope.permisosAsignados = [];
	$scope.usuarios = [];


	$scope.cargarPermisos = function(){
		var url = '/administracion/modulo/cargarPermisos';
		var data = { 
			sistema : $('#modulo').val(),
			usuario : $('#usuario').val()
		};

		$http.post(url, data).
			success(function(data, status, headers, config) {

		    	$scope.permisosAsignados = data['asignados'];

		    	$scope.permisosDisponibles = data['disponibles'];
		    	
			});
	}

	$scope.concederAcceso = function(pvista, precurso, ptitulo){
		
		if(confirm("Esta seguro?")){
			var url = '/administracion/modulo/concederAcceso';
			var data = { 
				sistema : $('#modulo').val(),
				vista : pvista,
				recurso : precurso,
				titulo : ptitulo,
				usuario : $('#usuario').val()
			};

			$http.post(url, data).
				success(function(data, status, headers, config) {
			    	$scope.cargarPermisos();
				});
		}
	}

	$scope.denegarAcceso = function(pvista, precurso, ptitulo){
		
		if(confirm("Esta seguro?")){
			var url = '/administracion/modulo/denegarAcceso';
			var data = { 
				sistema : $('#modulo').val(),
				vista : pvista,
				recurso : precurso,
				titulo : ptitulo,
				usuario : $('#usuario').val()
			};

			$http.post(url, data).
				success(function(data, status, headers, config) {
			    	$scope.cargarPermisos();
				});
		}
	}

});

$(document).ready(function(){
	$('#modulo').bind('change', cargarUsuarios);
	$('#modulo').change();
});


function cargarUsuarios()
{

	$('#usuario option').remove();

	var uri = '/administracion/modulo/usuariosPorModulo';
	var moduloSeleccionado = $('#modulo').val();
	var data = 
	{
		modulo : moduloSeleccionado
	}

	$.post(uri, data, function(d){
		for(var i = 0; i < d.modulos.length ; i++)
		{
			$('#usuario').append('<option>'+d.modulos[i].usuario+'</option>');
		}

		$('#usuario').change();

	}, "json");
}