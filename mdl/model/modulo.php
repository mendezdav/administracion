<?php

class moduloModel extends object {

    public function __construct() {
        
    }

    public function actualizar_campo($campo, $valor) {
        $query = "UPDATE campo_actualizacion set actualizar=$valor WHERE nombre_campo='{$campo}'";
        data_model()->executeQuery($query);
    }

    public function habilitar() {
        $query = "UPDATE cliente SET requiere_actualizar = true;";
        data_model()->executeQuery($query);
    }

    public function obtenerUsuarios($modulo){

        $query = "SELECT * FROM empleado INNER JOIN cliente on id_datos = codigo_afiliado WHERE modulo='{$modulo}'";

        return data_model()->cacheQuery($query);
    }

    public function validarCodigo($modulo, $cliente){

        $query = "SELECT * FROM empleado WHERE id_datos=$cliente AND modulo='{$modulo}'";

        data_model()->executeQuery($query);

        if(data_model()->getNumRows()>0){
            return true;
        }

        return false;
    }

    public function es_empleado($id_datos){

        $query = "SELECT * FROM empleado WHERE id_datos='{$id_datos}'";

        data_model()->executeQuery($query);

        if(data_model()->getNumRows()>0){
            return true;
        }

        return false;
    }

    public function revocarPermisos($usuario,$modulo){
        $query = "DELETE FROM acceso_sistema WHERE usuario='{$usuario}' AND sistema='{$modulo}'";
        data_model()->executeQuery($query);
    } 

    public function usuariosPorModulo($modulo){
        $query = "SELECT usuario FROM empleado WHERE modulo='{$modulo}'";

        data_model()->executeQuery($query);

        $res = array();
        
        while ($row = data_model()->getResult()->fetch_assoc()) {
            $res[] = $row;
        }

        return $res;
    }

    public function validarUsuario($modulo, $usuario){

        $query = "SELECT * FROM empleado WHERE usuario='{$usuario}' AND modulo='{$modulo}'";

        data_model()->executeQuery($query);

        if(data_model()->getNumRows()>0){
            return true;
        }

        return false;
    }

    public function cargar_campos() {
        $query = "SELECT * FROM campo_actualizacion";
        data_model()->executeQuery($query);
        $retArray = array();
        while ($res = data_model()->getResult()->fetch_assoc()) {
            $retArray[] = $res;
        }
        echo json_encode($retArray);
    }

    public function obtener_actualizables() {
        $query = "SELECT * FROM campo_actualizacion WHERE actualizar=1";
        data_model()->executeQuery($query);
        $retArray = array();
        while ($res = data_model()->getResult()->fetch_assoc()) {
            $retArray[] = $res;
        }
        return $retArray;
    }

    public function inboxPendientes(){
        $usuario = Session::singleton()->getUser();
        $query   = "SELECT COUNT(*) AS noleidos FROM inbox WHERE destinatario='{$usuario}' AND leido = 0";
        data_model()->executeQuery($query);
        $resp    = data_model()->getResult()->fetch_assoc();
        echo json_encode($resp);
    }

    public function inboxPreview(){
        $usuario = Session::singleton()->getUser();
        $query   = "SELECT * FROM inbox WHERE destinatario='{$usuario}' AND archivado = 0 ORDER BY id DESC LIMIT 5 ";
        data_model()->executeQuery($query);
        $resp = array();
        while($rest    = data_model()->getResult()->fetch_assoc()){
            $resp[] = $rest;
        }
        echo json_encode($resp);   
    }

    public function leerBuzon($leidos, $cantidad){
        $usuario = Session::singleton()->getUser();
        $query   = "SELECT * FROM inbox WHERE destinatario='{$usuario}' AND archivado = 0 ORDER BY id DESC LIMIT $leidos, $cantidad";
        data_model()->executeQuery($query);
        $resp = array();
        while($rest    = data_model()->getResult()->fetch_assoc()){
            $resp[] = $rest;
        }
        echo json_encode($resp);   
    }

    public function leerBuzonArchivados($leidos, $cantidad){
        $usuario = Session::singleton()->getUser();
        $query   = "SELECT * FROM inbox WHERE destinatario='{$usuario}' AND archivado = 1 ORDER BY id DESC LIMIT $leidos, $cantidad";
        data_model()->executeQuery($query);
        $resp = array();
        while($rest    = data_model()->getResult()->fetch_assoc()){
            $resp[] = $rest;
        }
        echo json_encode($resp);   
    }

    public function leerBuzonSalida($leidos, $cantidad){
        $usuario = Session::singleton()->getUser();
        $query   = "SELECT * FROM inbox WHERE remitente='{$usuario}' ORDER BY id DESC LIMIT $leidos, $cantidad";
        data_model()->executeQuery($query);
        $resp = array();
        while($rest    = data_model()->getResult()->fetch_assoc()){
            $resp[] = $rest;
        }
        echo json_encode($resp);   
    }

}

?>