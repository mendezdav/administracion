<?php

import('mdl.model.modulo');
import('mdl.view.modulo');

if (!Session::ValidateSession())
    HttpHandler::redirect(DEFAULT_DIR);

class moduloController extends controller {

    private $MODULES = array(
        'inventario' => array('acces' => false, 'subCategory' => array('colores' => false, 'tallas' => false)),
        'factura' => array('acces' => false, 'subCategory' => array()),
        'cliente' => array('acces' => false, 'subCategory' => array('ficha' => false, 'listado' => false))
    );


    public function principal(){

        $this->view->principal();
    }

    public function gestionDeUsuarios(){

        $this->view->gestionDeUsuarios();
    }

    public function gestionDePermisos(){

        $this->view->gestionDePermisos();
    }
    
    public function agentes(){

        $this->view->agentes();
    }
    
    public function zonas(){

        $this->view->zonas();
    }
    
    public function administrar_zonas(){
        $cache = array();
        $cache[0] = data_model()->cacheQuery("SELECT id, nombre FROM municipio WHERE zona = 0");
        $cache[1] = data_model()->cacheQuery("SELECT codru, nombre FROM ruta");
        $cache[2] = data_model()->cacheQuery("SELECT codru, nombre FROM ruta");
        $cache[3] = data_model()->cacheQuery("SELECT municipio.id as id, municipio.nombre as nombre, ruta.nombre as zona FROM municipio JOIN ruta ON zona=codru WHERE zona != 0");
        $this->view->administrar_zonas($cache);
    }
    
    public function administrar_agentes(){
        $cache = array();
        $cache[0] = data_model()->cacheQuery("SELECT codru, nombre FROM ruta WHERE agente = 0");
        $cache[1] = data_model()->cacheQuery("SELECT cage, nombre FROM agente");
        $cache[2] = data_model()->cacheQuery("SELECT cage, nombre FROM agente");
        $cache[3] = data_model()->cacheQuery("SELECT codru, ruta.nombre as nombre, agente.nombre as agente FROM ruta JOIN agente ON cage=agente WHERE agente != 0");
        $this->view->administrar_agentes($cache);
    }
    
    public function actualizar_zona(){
        if(isset($_POST)&&!empty($_POST)){
           $municipio = $_POST['municipio'];
           $zona = $_POST['zona'];
           
           $query = "UPDATE municipio SET zona=$zona WHERE id = $municipio";
           
           data_model()->executeQuery($query);
           echo json_encode(array("msg"=>""));  
        
        }else{
            HttpHandler::redirect('/administracion/error/not_found');
        }
    }
    
    public function actualizar_agente(){
        if(isset($_POST)&&!empty($_POST)){
           $zona = $_POST['zona'];
           $agente = $_POST['agente'];
           
           $query = "UPDATE ruta SET agente=$agente WHERE codru = $zona";
           
           data_model()->executeQuery($query);
           echo json_encode(array("msg"=>""));  
        
        }else{
            HttpHandler::redirect('/administracion/error/not_found');
        }
    }
    
    public function guardar_agente(){
        if(isset($_POST)&&!empty($_POST)){
            $response = array();
            $response['exists'] = false;
            $codigo = $_POST['i_codigo'];
            $nombre = $_POST['i_nombre'];
            $comision = $_POST['i_comision'];
            
            $agente = $this->model->get_child('agente');
            $agente->setVirtualId("cage");
            if(!$agente->exists($codigo)){
                $agente->setVirtualId("id");
                $agente->get(0);
                $agente->cage = $codigo;
                $agente->nombre = $nombre;
                $agente->pcom = $comision;
                $agente->save();
            }else{
                $response['exists'] = true;
            }
            
            echo json_encode($response);
        }else{
            HttpHandler::redirect('/administracion/error/not_found');
        }
    }
    
    public function guardar_zona(){
        if(isset($_POST)&&!empty($_POST)){
            $response = array();
            $response['exists'] = false;
            $codigo = $_POST['i_codigo'];
            $nombre = $_POST['i_nombre'];
            $descripcion = $_POST['i_descripcion'];
            
            $agente = $this->model->get_child('ruta');
            $agente->setVirtualId("codru");
            if(!$agente->exists($codigo)){
                $agente->setVirtualId("id");
                $agente->get(0);
                $agente->codru = $codigo;
                $agente->nombre = $nombre;
                $agente->descripcion = $descripcion;
                $agente->save();
            }else{
                $response['exists'] = true;
            }
            
            echo json_encode($response);
        }else{
            HttpHandler::redirect('/administracion/error/not_found');
        }
    }

    public function usuariosPorModulo(){
        if(isset($_POST['modulo'])&&!empty($_POST['modulo'])){
           
            $response = array();

            $response['modulos'] = array();

            $response['modulos'] = $this->model->usuariosPorModulo($_POST['modulo']);

            echo json_encode($response);
        }
    }

    public function cargarPermisos(){
        $params  = json_decode(file_get_contents('php://input'),true);
        $sistema = $params['sistema'];
        $usuario = $params['usuario'];

        $query = "SELECT id, sistema, vista, recurso, titulo,  CONCAT(SUBSTRING(titulo,1,20),'...') AS titulo_c FROM vista_sistema WHERE sistema='{$sistema}' AND CONCAT(sistema,vista,recurso,'{$usuario}') NOT IN (SELECT CONCAT(sistema, vista, recurso, usuario) FROM acceso_sistema WHERE sistema='{$sistema}' AND usuario = '{$usuario}')";
        data_model()->executeQuery($query);

        $response = array();

        while ($row = data_model()->getResult()->fetch_assoc()) {
            $response['disponibles'][] = $row;
        }

        $query = "SELECT id, sistema, vista, recurso, titulo,  CONCAT(SUBSTRING(titulo,1,20),'...') AS titulo_c FROM acceso_sistema WHERE sistema='{$sistema}' AND usuario = '{$usuario}'";
        
        data_model()->executeQuery($query);

        while ($row = data_model()->getResult()->fetch_assoc()) {
            $response['asignados'][] = $row;
        }

        echo json_encode($response);
    }

    public function concederAcceso(){
        $params  = json_decode(file_get_contents('php://input'),true);
        $sistema = $params['sistema'];
        $usuario = $params['usuario'];
        $vista = $params['vista'];
        $recurso = $params['recurso'];
        $titulo = $params['titulo'];

        $query = "INSERT INTO acceso_sistema (usuario, sistema, vista, recurso, titulo) VALUES ('{$usuario}','{$sistema}','{$vista}','{$recurso}','{$titulo}')";
        
        data_model()->executeQuery($query);

        $response = array("status"=>"200");

        echo json_encode($response);
    }

    public function denegarAcceso(){
        $params  = json_decode(file_get_contents('php://input'),true);
        $sistema = $params['sistema'];
        $usuario = $params['usuario'];
        $vista = $params['vista'];
        $recurso = $params['recurso'];
        $titulo = $params['titulo'];

        $query = "DELETE FROM acceso_sistema WHERE usuario='{$usuario}' AND sistema='{$sistema}' AND vista='{$vista}' AND recurso='{$recurso}'";
        
        data_model()->executeQuery($query);

        $response = array("status"=>"200");

        echo json_encode($response);
    }

    public function obtenerUsuarios(){
        if(isset($_POST['modulo']) && !empty($_POST['modulo'])){
            $response = array();

            $response['content'] = "content preview for module ".$_POST['modulo'];

            $userList = $this->model->obtenerUsuarios($_POST['modulo']);

            $response['content'] = $this->view->obtenerUsuarios($userList);

            echo json_encode($response);
        }
    }

    public function validarCodigo(){
        if(isset($_POST['modulo'])&&isset($_POST['cliente'])){
            $response = array();

            $response['existe'] = $this->model->validarCodigo($_POST['modulo'], $_POST['cliente']);

            echo json_encode($response);
        }   
    }

    public function validarUsuario(){
        if(isset($_POST['modulo'])&&isset($_POST['usuario'])){
            $response = array();

            $response['existe'] = $this->model->validarUsuario($_POST['modulo'], $_POST['usuario']);

            echo json_encode($response);
        }   
    }

    public function agregarUsuario(){

        $empleado = $this->model->get_child('empleado');
        $cliente = $this->model->get_sibling('cliente');
        $empleado->get(0);
        $empleado->id_datos = $_POST['cliente'];
        $empleado->modulo = $_POST['modulo'];
        $empleado->usuario = $_POST['usuario'];
        $empleado->clave = cifrar_RIJNDAEL_256($_POST['usuario']);
        $empleado->activo = 1;
        $empleado->permiso = 0;

        $empleado->save();

        $cliente->get($empleado->id_datos);
        $cliente->empleado = 1;
        $cliente->save();

        echo json_encode(array("msg"=>''));
    }

    public function eliminarUsuario(){
        $empleado = $this->model->get_child('empleado');
        $empleado->get($_POST['id_reg']);
        $codigo_afiliado = $empleado->id_datos;
        $usuario = $empleado->usuario;
        if($usuario!="admin"){
            $modulo = $empleado->modulo;
            $empleado->delete($_POST['id_reg']);

            if(!$this->model->es_empleado($codigo_afiliado)){
                $cliente = $this->model->get_sibling("cliente");
                $cliente->get($codigo_afiliado);
                $cliente->empleado = "0";
                $cliente->save();
            }

            $this->model->revocarPermisos($usuario,$modulo);

        }

        echo json_encode(array("msg"=>''));   
    }

    public function listar() {
        $user = Session::singleton()->getUser();
        
        $sys = $this->model->get_child('system');
        $sys->get(1);

        $nombreEmpresa    = $sys->nombre_comercial;
        $direccionEmpresa = $sys->direccion; 
        $telefonoEmpresa  = $sys->telefono;
        $faxEmpresa       = $sys->fax; 
        $this->view->listar($user, $nombreEmpresa, $direccionEmpresa, $telefonoEmpresa, $faxEmpresa);
    }

    public function opcionesDeSistema() {

        $level = Session::singleton()->getLevel();
        $user = Session::singleton()->getUser();
        $cache = array();
        $cache[0] = $this->model->get_child('system')->get_list();
        $this->view->opcionesDeSistema($user, $cache);
    }

    public function politicas_de_actualizacion() {
        if (Session::singleton()->getLevel() == 1) {
            $this->view->politicas_de_actualizacion(Session::singleton()->getUser());
        } else {
            HttpHandler::redirect('/administracion/modulo/listar?error=1530');
        }
    }

    public function actualizar_campo() {
        if (isset($_POST) && !empty($_POST)) {
            $json = json_decode(stripslashes($_POST["campos"]));
            foreach ($json as $pp) {
                $campo = $pp->{'campo'};
                $valor = ($pp->{'valor'}) ? true : 0;
                $this->model->actualizar_campo($campo, $valor);
            }
            $this->model->habilitar();
        }
    }

    public function cargar_campos() {
        $this->model->cargar_campos();
    }

    public function requiere_actualizar() {
        $response = array();
        $cliente = $this->model->get_sibling("cliente");
        if ($_POST['cliente'] != 0) {
            $cliente->get(addslashes($_POST['cliente']));
            $response['actualizar'] = ( $cliente->requiere_actualizar ) ? true : false;
        }
        echo json_encode($response);
    }

    public function efectuar_actualizacion() {
        $cliente = $this->model->get_sibling("cliente");
        $cliente->get(addslashes($_POST['cliente']));

        $json = json_decode($_POST['update']);
        foreach ($json as $row) {
            $cliente->{$row->{'campo'}} = $row->{'valor'};
        }
        $cliente->requiere_actualizar = '0';
        $cliente->save();
    }

    public function cambiar_clave() {
        $system = $this->model->get_child('system');
        $system->get(1);
        $system->set_attr('clave', md5($_POST['clave']));
        $system->save();

        echo json_encode(array("mesg"=>""));
    }

    public function cambiar_clave_usuario() {
        $usuario = $this->model->get_child('empleado');
        $usuario->get($_POST['usuario']);
        $usuario->set_attr('clave', cifrar_RIJNDAEL_256($_POST['clave']));
        $usuario->save();
    }

    public function CheckIfModuleEnabled($moduleName) {
        if (isset($this->MODULES[$moduleName])) {
            return $this->MODULES[$moduleName]['acces'];
        } else {
            return false;
        }
    }

    public function system_save() {
        $system = $this->model->get_child('system');

        $system->get($_POST['id']);
        $system->change_status($_POST);
        $system->save();

        HttpHandler::redirect('/administracion/modulo/opcionesDeSistema');
    }

    public function CheckIfModuleEnabledStr($moduleName) {
        $ret = array();
        $ret['Module'] = $moduleName;
        if (isset($this->MODULES[$moduleName])) {
            $ret['Result'] = $this->MODULES[$moduleName]['acces'];
        } else {
            $ret['Result'] = false;
        }
        echo json_encode($ret);
    }

    public function serializeModules() {
        return serialize($this->MODULES);
    }

    public function CheckIfSubModuleEnabled($moduleName, $subModuleName) {
        if (isset($this->MODULES[$moduleName]['subCategory'][$subModuleName])) {
            return $this->MODULES[$moduleName]['subCategory'][$subModuleName];
        } else {
            return false;
        }
    }

    public function crearSesion($nm) {
        $_SESSION[$nm] = true;
        HttpHandler::redirect('/administracion/' . $nm . '/principal');
    }

    public function destruirSesion($nm) {
        unset($_SESSION[$nm]);
        HttpHandler::redirect('/administracion/modulo/listar');
    }

    public function cuenta() {
        $this->view->cuenta(Session::singleton()->getUser());
    }

    public function autorizacion() {
        $clave = $_POST['clave'];
        $ret = array();
        $ret['auth'] = false;
        $system = $this->model->get_child('system');
        $system->get(1);
        if (md5($clave) == $system->get_attr('clave')) {
            $ret['auth'] = true;
        }

        echo json_encode($ret);
    }

    public function inboxPendientes(){

        $this->model->inboxPendientes();
    }

    public function inboxPreview(){
        
        $this->model->inboxPreview();
    }

    public function leerBuzon(){
        
        $this->model->leerBuzon($_POST['leidos'], $_POST['cantidad']);
    }

    public function leerBuzonSalida(){
        
        $this->model->leerBuzonSalida($_POST['leidos'], $_POST['cantidad']);
    }

    public function leerBuzonArchivados(){
        
        $this->model->leerBuzonArchivados($_POST['leidos'], $_POST['cantidad']);
    }

    public function inboxRead(){
        $inbox = $this->model->get_child('inbox');
        if(isset($_GET['inboxId'])){
            $inbox->get($_GET['inboxId']);
            $tipo = ( isset($_GET['type']) ) ? $_GET['type']: "in";
            if($tipo=="in"){
                if($inbox->exists($_GET['inboxId'])){
                    $inbox->leido = "1";
                    $inbox->leido_a_las = date("Y-m-d H:i:s");
                    $inbox->save();
                }
            }
            $this->view->inboxRead($inbox, $tipo);
        }
    }

    public function inboxReadOut(){
        $inbox = $this->model->get_child('inbox');
        if(isset($_GET['inboxId'])){
            $inbox->get($_GET['inboxId']);
            $this->view->inboxReadOut($inbox);
        }
    }

    public function enviarMensaje(){
        if(isset($_POST) && !empty($_POST)){
            $mensaje = $_POST['mensaje'];
            $destinatario = $_POST['destinatario'];
            $titulo = $_POST['titulo'];

            $inbox = $this->model->get_child('inbox');
            $inbox->mensaje = str_replace("\n", "<br/>", $mensaje);
            $inbox->destinatario = $destinatario;
            $inbox->remitente = Session::singleton()->getUser();
            $inbox->fecha = date("Y-m-d");
            $inbox->titulo = $titulo;
            $inbox->save();
            HttpHandler::redirect('/administracion/modulo/inboxRead?inboxId=0&Compose=true');
        }
    }

    public function eliminar_mensaje(){
        if(isset($_GET['id_inbox']) && !empty($_GET['id_inbox'])){
            $id_inbox = $_GET['id_inbox'];
            $inbox = $this->model->get_child('inbox');
            if($inbox->exists($id_inbox)){
                $inbox->get($id_inbox);
                $inbox->delete($id_inbox);
            }
        }

       HttpHandler::redirect('/administracion/modulo/inboxRead?inboxId=0&Compose=true');
    }

    public function archivar_mensaje(){
        if(isset($_GET['id_inbox']) && !empty($_GET['id_inbox'])){
            $id_inbox = $_GET['id_inbox'];
            $inbox = $this->model->get_child('inbox');
            if($inbox->exists($id_inbox)){
                $inbox->get($id_inbox);
                $inbox->archivado = "1";
                $inbox->save();
            }
        }

       HttpHandler::redirect('/administracion/modulo/inboxRead?inboxId=0&Compose=true');
    }

    public function desarchivar_mensaje(){
        if(isset($_GET['id_inbox']) && !empty($_GET['id_inbox'])){
            $id_inbox = $_GET['id_inbox'];
            $inbox = $this->model->get_child('inbox');
            if($inbox->exists($id_inbox)){
                $inbox->get($id_inbox);
                $inbox->archivado = "0";
                $inbox->save();
            }
        }

       HttpHandler::redirect('/administracion/modulo/inboxRead?inboxId=0&Compose=true');
    }

    public function alerta(){

        $this->view->alerta();
    }

}

?>